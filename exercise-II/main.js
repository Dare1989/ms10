let num = parseInt(prompt("Enter a number to see what day it is!"));
let text = document.createElement("h1");
document.body.appendChild(text);
if (num == 1) {
  text.innerText = "It’s monday!";
  text.style.color = "green";
} else if (num == 2) {
  text.innerText = "It’s tuesday!";
  text.style.color = "green";
} else if (num == 3) {
  text.innerText = "It’s wednesday!";
  text.style.color = "green";
} else if (num == 4) {
  text.innerText = "It’s thursday!";
  text.style.color = "green";
} else if (num == 5) {
  text.innerText = "It’s friday!";
  text.style.color = "green";
} else if (num == 6) {
  text.innerText = "It’s saturday!";
  text.style.color = "green";
} else if (num == 7) {
  text.innerText = "It’s sunday!";
  text.style.color = "green";
} else {
  text.innerText = "Wrong data!";
  text.style.color = "red";
}
